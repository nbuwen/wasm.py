import logging
from argparse import ArgumentParser

from waspy import Loader, Interpreter


def print_help():
    print("type '?' to show this message")
    print("type FUNCTION ARGS* to call the")
    print("    function called FUNCTION")
    print("    with any arguments")
    print("press CTRL+C to quit")


def repl(ip):
    while True:
        cmd = input('> ')
        if not cmd:
            continue
        if cmd == '?':
            print_help()
            continue

        cmd, *args = cmd.split()

        try:
            result = interpreter.call(cmd, *args)
        except (KeyError, AssertionError) as e:
            print('!', *e.args)
        else:
            print('=', result)


parser = ArgumentParser(description="Wasp test tool")
parser.add_argument('file', metavar='FILE', help="path to the .wasm file")
parser.add_argument('--log', choices='FATAL CRITICAL ERROR WARNING INFO DEBUG INSTR'.split(),
                    help="minimum logging level to report", default='INFO')
parser.add_argument('--asm', '-a', action='store_true', default=False,
                    help="show annotated assembly")

if __name__ == '__main__':
    args = parser.parse_args()

    logging.basicConfig(level=args.log)

    loader = Loader.from_file(args.file)

    try:
        module = loader.load()
    except Exception:
        loader.index -= 1
        raise
    finally:
        if args.asm:
            loader.print_code()

    interpreter = Interpreter(module)
    if module.exports:
        print(f"Module exports {len(module.exports)} symbols:")
        for symbol, object in module.exports.items():
            print(f"  {symbol:<10}: {object.__class__.__name__}")
    else:
        print("Module does not export any symbols")

    for f in module.functions:
        print(f)

    try:
        repl(interpreter)
    except KeyboardInterrupt:
        print("")
