WASM Interpreter written in python
==================================

Working examples:
-----------------

- `add.wasm`
    - `add 1 2` `->` `3`
- `fib.wasm`
    - `fib 10` `->` `89`
    - `fib 20` `->` `10946` (noticeable delay)
- `sub.wasm`
    - `sub 20 2` `->` `18`