from dataclasses import dataclass, field
from typing import List, Dict, Any

from .parts import Function


@dataclass
class Module:
    functions: List[Function] = field(default_factory=list)
    exports: Dict[str, Any] = field(default_factory=dict)

    def call(self, function):
        function = self.exports[function]
