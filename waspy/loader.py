from dataclasses import dataclass, field
from contextlib import contextmanager
from logging import getLogger
from typing import Dict, List

from .util import register, symbol, Distance
from .module import Module
from .parts import PrimitiveMixin, TypeMixin, Function

logger = getLogger(__name__)


@register('sections')
class ModuleMixin:
    def load(self):
        self.require(b'\x00')
        self.annotate(['initial 0'])
        self.require(b'asm')
        self.annotate('asm', prefix='asm ')
        self.require(b'\x01\x00\x00\x00')
        self.annotate('1000', prefix="version ")

        while not self.done:
            index = self.index
            section = self.section()
            self.annotation[index] = section

        return self.module

    @symbol(0)
    def custom_section(self, size):
        with self.save_distance() as distance:
            name = bytes(self.vector(self.byte)).decode()
        logger.debug(f"custom section {name!r}")
        logger.debug(b'  ' + self.raw_bytes(size - distance.value))

    @symbol(1)
    def type_section(self, size):
        for signature in self.vector(self.type):
            self.function_signatures.append(signature)
            logger.debug(f"found function signature: {Function.from_tuple(signature)}")

    @symbol(3)
    def function_section(self, size):
        for signature_index in self.vector(self.unsigned_leb):
            signature = self.function_signatures[signature_index]
            self.module.functions.append(Function.from_tuple(signature))
            logger.debug(f"declared function: {self.module.functions[-1]}")

    @symbol(7)
    def export_section(self, size):
        exports = self.vector(self.export_item)
        for name, object in exports:
            self.module.exports[name] = object
            logger.debug(f"exporting {name}")
        for index, object in enumerate(self.module.functions):
            self.module.exports[f'#{index}'] = object

    def export_item(self):
        name = bytes(self.vector(self.byte)).decode()
        type = [
            self.module.functions
        ][self.byte()]
        index = self.unsigned_leb()
        return name, type[index]

    @symbol(10)
    def code_section(self, size):
        codes = self.vector(self.code_item)
        functions = self.module.functions
        logger.debug(f"found {len(codes)} code items for {len(functions)} functions")
        for index, (function, (locals, expression)) in enumerate(zip(functions, codes)):
            function.locals = locals
            function.expression = expression
            function.index = index

    def code_item(self):
        size = self.unsigned_leb()
        logger.debug(f"found code of size {size}")
        with self.save_distance() as distance:
            locals = self.vector(self.local_item)
        remaining = size - distance.value
        return locals, self.raw_bytes(remaining)

    def local_item(self):
        return self.unsigned_leb(), self.type()

    def section(self):
        index = self.byte()
        i = self.index
        size = self.unsigned_leb()
        self.annotation[i] = f'size {size}'
        try:
            section = self.sections[index]
        except IndexError:
            raise IndexError(f"No Section with code 0x{index:x} found!") from None
        section(self, size)
        return section.__name__


class ControlMixin:
    @contextmanager
    def save_distance(self):
        distance = Distance(self.index)
        yield distance
        distance.end = self.index


@dataclass
class Loader(PrimitiveMixin, ModuleMixin, ControlMixin, TypeMixin):
    assembly: bytes
    index = 0
    module: Module = field(default_factory=Module)
    function_signatures: List = field(default_factory=list)
    annotation: Dict = field(default_factory=dict)

    @classmethod
    def from_file(cls, file_name):
        with open(file_name, 'rb') as fin:
            return cls(fin.read())

    @property
    def done(self):
        return self.index >= len(self.assembly)

    def print_code(self):
        for i, byte in enumerate(self.assembly):
            prefix = '->' if i == self.index else '  '
            print(f"{prefix} {byte:03} 0x{byte:02x} {self.annotation.get(i, '')}")

    def annotate(self, string, prefix=''):
        base = self.index - len(string)
        for i, char in enumerate(string, start=base):
            self.annotation[i] = f'{prefix}{char}'
