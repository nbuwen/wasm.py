from struct import pack, unpack
import operator
import math


def operator_rotate_left(operand, shift):
    return operand


def operator_rotate_right(operand, shift):
    return operand


def operator_copy_sign(operand):
    if operand > 0:
        return 1
    elif operand < 0:
        return -1
    return 0


def operator_eq_zero(operand):
    return operand == 0


def operator_count_leading_zeros(operand):
    raise NotImplementedError('count leading zeros')


def operator_count_trailing_zeros(operand):
    raise NotImplementedError('count trailing zeros')


def operator_count_ones(operand):
    raise NotImplementedError('count ones')


def operator_wrap(operand):
    return operand & 0xffff_ffff


def duplicate(dict, start, end, target):
    for key in range(end - start + 1):
        dict[key + target] = dict[key + start]


def operator_reinterpret(source, target):
    def op(value):
        return unpack('<' + target, pack('<' + source, value))
    return op


BINARY_INSTRUCTIONS = {
    0x46: operator.eq,
    0x47: operator.ne,
    0x48: operator.lt, 0x49: operator.lt,
    0x4a: operator.gt, 0x4b: operator.gt,
    0x4c: operator.le, 0x4d: operator.le,
    0x4e: operator.ge, 0x4f: operator.ge,

    0x5b: operator.eq,
    0x5c: operator.ne,
    0x5d: operator.lt,
    0x5e: operator.gt,
    0x5f: operator.le,
    0x60: operator.ge,

    0x6a: operator.add,
    0x6b: operator.sub,
    0x6c: operator.mul,
    0x6d: operator.floordiv, 0x6e: operator.floordiv,
    0x6f: operator.mod, 0x70: operator.mod,
    0x71: operator.and_,
    0x72: operator.or_,
    0x73: operator.xor,
    0x74: operator.lshift,
    0x75: operator.rshift, 0x76: operator.rshift,
    0x77: operator_rotate_left,
    0x78: operator_rotate_right,

    0x92: operator.add,
    0x93: operator.sub,
    0x94: operator.mul,
    0x95: operator.truediv,
    0x96: min,
    0x97: max,
    0x98: operator_copy_sign,
}

duplicate(BINARY_INSTRUCTIONS, 0x46, 0x4f, 0x51)
duplicate(BINARY_INSTRUCTIONS, 0x5b, 0x60, 0x61)
duplicate(BINARY_INSTRUCTIONS, 0x6a, 0x78, 0x7c)
duplicate(BINARY_INSTRUCTIONS, 0x92, 0x98, 0xa0)

MISSING = {
    0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b,
    0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
    0x1c, 0x1d, 0x1e, 0x1f,
    0x25, 0x26, 0x27,

}


def add_binary_instructions(cls):
    for instruction, op in BINARY_INSTRUCTIONS.items():
        def op_function(self, op=op):
            right = self.stack.pop()
            left = self.stack.pop()
            self.stack.append(op(left, right))
        op_function.__name__ = op.__name__
        cls.instructions[instruction] = op_function
    return cls


def add_memory_instructions(cls):
    for instruction in range(0x28, 0x35 + 1):
        def load_function(self):
            raise NotImplementedError('load {instruction:x}')
        load_function.__name__ += str(0x35 - instruction)
        cls.instructions[instruction] = load_function

    for instruction in range(0x36, 0x3e + 1):
        def store_function(self):
            raise NotImplementedError('store {instruction:x}')
        store_function.__name__ += str(0x3e - instruction)
        cls.instructions[instruction] = store_function

    return cls


UNARY_INSTRUCTIONS = {
    0x45: operator_eq_zero,
    0x50: operator_eq_zero,
    0x67: operator_count_leading_zeros, 0x79: operator_count_leading_zeros,
    0x68: operator_count_trailing_zeros, 0x7a: operator_count_trailing_zeros,
    0x69: operator_count_ones, 0x7b: operator_count_ones,

    0x8b: operator.abs,
    0x8c: operator.neg,
    0x8d: math.ceil,
    0x8e: math.floor,
    0x8f: math.trunc,
    0x90: round,
    0x91: math.sqrt,

    0xa7: operator_wrap,
    0xa8: int, 0xa9: int, 0xaa: int, 0xab: int,
    0xac: int, 0xad: int,
    0xae: int, 0xaf: int, 0xb0: int, 0xb1: int,
    0xb2: float, 0xb3: float, 0xb4: float, 0xb5: float,
    0xb6: float,
    0xb7: float, 0xb8: float, 0xb9: float, 0xba: float,
    0xbb: float,
    0xbc: operator_reinterpret('f', 'I'),
    0xbd: operator_reinterpret('d', 'Q'),
    0xbe: operator_reinterpret('I', 'f'),
    0xbf: operator_reinterpret('Q', 'd'),
}


duplicate(UNARY_INSTRUCTIONS, 0x8b, 0x91, 0x99)


def add_unary_instructions(cls):
    for instruction, op in UNARY_INSTRUCTIONS.items():
        def op_function(self, op=op):
            self.stack.append(op(self.stack.pop()))
        op_function.__name__ = op.__name__
        cls.instructions[instruction] = op_function
    return cls


__all__ = [
    'add_binary_instructions',
    'add_memory_instructions',
    'add_unary_instructions'
]
