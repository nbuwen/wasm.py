from dataclasses import dataclass
from logging import addLevelName, Logger


def symbol(byte):
    def decorator(function):
        if not hasattr(function, 'symbol'):
            function.symbol = []
        function.symbol.append(byte)
        return function
    return decorator


def register(name):
    def decorator(cls):
        lookup = {}
        setattr(cls, name, lookup)
        for key, value in cls.__dict__.items():
            try:
                for symbol in value.symbol:
                    lookup[symbol] = value
            except AttributeError:
                pass
        return cls
    return decorator


@dataclass
class Distance:
    start: int
    end: int = None

    @property
    def value(self):
        return self.end - self.start


LOG_LEVEL_INSTR = 5


addLevelName(LOG_LEVEL_INSTR, 'INSTR')


def log_instruction(self, scopes, message, *args, **kwargs):
    indent = len(scopes.scopes)
    if self.isEnabledFor(LOG_LEVEL_INSTR):
        self._log(LOG_LEVEL_INSTR, '  ' * indent + message, args, **kwargs)


Logger.instruction = log_instruction
