from struct import Struct
from dataclasses import dataclass
from typing import List, Type, Optional

from .util import register, symbol


@dataclass
class Function:
    arguments: List[Type]
    result: Optional[Type]
    locals = None
    expression = None
    index = None

    @classmethod
    def from_tuple(cls, signature):
        arguments, results = signature
        result = results[0] if results else None
        return cls(arguments, result)

    def __str__(self):
        args = ', '.join(type.__name__ for type in self.arguments)
        result = 'void' if self.result is None else self.result.__name__
        return f'({args}) -> {result}'


class PrimitiveMixin:
    def byte(self):
        byte = self.assembly[self.index]
        self.index += 1
        return byte

    def raw_bytes(self, n):
        bytes = self.assembly[self.index:self.index + n]
        self.index += n
        return bytes

    def skip(self, n=1):
        self.index += n

    def require(self, bytes):
        assert self.raw_bytes(len(bytes)) == bytes

    def unsigned_leb(self):
        byte = self.byte()
        result = byte & 0x7f
        while byte & 0x80:
            byte = self.byte()
            result = (result << 7) | (byte & 0x7b)
        return result

    def signed_leb(self, size=32):
        result = 0
        shift = 0

        byte = self.byte()
        result = byte & 0x7f
        while byte & 0x80:
            byte = self.byte()
            result = (result << 7) | (byte & 0x7b)
        if shift < size and byte & 0x40:
            result |= (~0 << shift)
        return result

    def unpack(self, what):
        struct = Struct('<' + what)
        values = struct.unpack_from(self.assembly, offset=self.index)
        self.index += struct.size
        return values


@register('types')
class TypeMixin:
    @symbol(0x40)
    def empty(self):
        return type(None)

    @symbol(0x60)
    def function_type(self):
        args = self.vector(self.type)
        result = self.vector(self.type)
        return args, result

    @symbol(0x7c)
    def double_type(self):
        return float

    @symbol(0x7d)
    def float_type(self):
        return float

    @symbol(0x7e)
    def long_type(self):
        return int

    @symbol(0x7f)
    def int_type(self):
        return int

    def vector(self, content):
        length = self.unsigned_leb()
        return [content() for _ in range(length)]

    def type(self):
        index = self.byte()
        try:
            type = self.types[index]
        except KeyError:
            raise KeyError(f"No type with code 0x{index:x} found!") from None
        return type(self)
