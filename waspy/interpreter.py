from dataclasses import dataclass, field
from typing import List
from logging import getLogger

from .module import Module, Function
from .util import register, symbol
from .parts import PrimitiveMixin, TypeMixin
from .instructions import add_unary_instructions, add_binary_instructions, add_memory_instructions
logger = getLogger(__name__)


@dataclass
class Scope:
    locals: List = field(default_factory=list)
    parent_index: int = -1
    jump_back: int = -1


@dataclass
class ScopeStack:
    scopes: List[Scope] = field(default_factory=list)
    current_function_index = 0

    def push(self, args, function, jump_back):
        scope = Scope(list(args), self.current_function_index, jump_back)
        self.current_function_index = function.index
        self.scopes.append(scope)

    def pop(self):
        scope = self.scopes.pop()
        self.current_function_index = scope.parent_index
        return scope

    def clear(self):
        self.scopes.clear()

    def __bool__(self):
        return bool(self.scopes)

    @property
    def current(self):
        return self.scopes[-1]


@add_memory_instructions
@add_unary_instructions
@add_binary_instructions
@register('instructions')
class InstructionMixin:
    @symbol(0x00)
    def unreachable(self):
        raise RuntimeError(f"Trap @{self.index}")

    @symbol(0x01)
    def nop(self):
        pass

    @symbol(0x02)
    def block(self):
        raise NotImplementedError('block')

    @symbol(0x03)
    def loop(self):
        raise NotImplementedError('loop')

    @symbol(0x04)
    def if_(self):
        self.type()

        if not self.stack.pop():
            while self.byte() != 0x0b:
                pass

    @symbol(0x05)
    def else_(self):
        raise NotImplementedError('else')

    @symbol(0x0c)
    def branch(self):
        raise NotImplementedError('branch')

    @symbol(0x0d)
    def branch_if(self):
        raise NotImplementedError('branch if')

    @symbol(0x0e)
    def branch_table(self):
        raise NotImplementedError('branch')

    @symbol(0x0b)
    @symbol(0x0f)
    def return_(self):
        self.cleanup_call()

    @symbol(0x10)
    def call(self):
        function = self.module.functions[self.unsigned_leb()]
        args = [self.stack.pop() for _ in function.arguments]
        self.prepare_call(args, function)

    @symbol(0x11)
    def call_indirect(self):
        raise NotImplementedError('call indirect')

    @symbol(0x1a)
    def drop(self):
        self.stack.pop()

    @symbol(0x1b)
    def select(self):
        true_ = self.stack.pop()
        false_ = self.stack.pop()
        if self.stack.pop() == 0:
            self.stack.append(true_)
        else:
            self.stack.append(false_)

    @symbol(0x20)
    def get_local(self):
        index = self.unsigned_leb()
        self.stack.append(self.scope.locals[index])

    @symbol(0x21)
    def set_local(self):
        index = self.unsigned_leb()
        self.scope.locals[index] = self.stack.pop()

    @symbol(0x22)
    def tee_local(self):
        index = self.unsigned_leb()
        self.scope.locals[index] = self.stack[-1]

    @symbol(0x23)
    def get_global(self):
        raise NotImplementedError('get global')

    @symbol(0x24)
    def set_global(self):
        raise NotImplementedError('set global')

    @symbol(0x3f)
    def memory_size(self):
        raise NotImplementedError('memory size')

    @symbol(0x40)
    def memory_grow(self):
        raise NotImplementedError('memory grow')

    @symbol(0x41)
    def const_int(self):
        self.stack.append(self.signed_leb(32))

    @symbol(0x42)
    def const_long(self):
        self.stack.append(self.signed_leb(64))

    @symbol(0x43)
    def const_float(self):
        self.stack.push(self.unpack('f'))

    @symbol(0x44)
    def const_double(self):
        self.stack.push(self.unpack('d'))


@dataclass
class Interpreter(InstructionMixin, PrimitiveMixin, TypeMixin):
    module: Module
    scopes: ScopeStack = field(default_factory=ScopeStack)
    stack: List = field(default_factory=list)
    assembly = None
    index = -1

    @property
    def scope(self):
        return self.scopes.current

    def prepare_call(self, args, function):
        logger.instruction(self.scopes, f"push scope {function.index}")
        self.scopes.push(args, function, self.index)
        self.index = 0
        self.assembly = function.expression

    def cleanup_call(self):
        scope = self.scopes.pop()
        self.index = scope.jump_back
        function = self.module.functions[scope.parent_index]
        self.assembly = function.expression
        logger.instruction(self.scopes, f"pop scope -> {scope.parent_index}")

    def call(self, function, *args):
        try:
            function = self.module.exports[function]
        except KeyError:
            raise AssertionError(f"Failed to call unknown symbol {function!r}") from None
        assert isinstance(function, Function), f"Failed to call symbol {function!r} - not a function"
        assert len(function.arguments) == len(args),\
            f"Failed to call function - got {len(args)} arguments instead of {len(function.arguments)}"

        try:
            converted_args = []
            for type, arg in zip(function.arguments, args):
                converted_args.append(type(arg))
        except ValueError:
            raise AssertionError(f"Failed to interpret {arg} as {type.__name__}") from None

        self.scopes.clear()
        self.stack.clear()

        self.index = None
        self.prepare_call(converted_args, function)
        result = self.interpret(function)
        # self.scopes.pop()

        if self.scopes:
            logger.warning("scope stack is not empty")
        if self.stack:
            logger.warning(f"value stack is not empty ({len(self.stack)})")

        return result

    def interpret(self, function):
        while self.index is not None:
            instruction = self.byte()
            instruction = self.instructions[instruction]
            logger.instruction(self.scopes, f"running {self.index:3} {instruction.__name__}")
            instruction(self)

        if function.result is not None:
            return self.stack.pop()
